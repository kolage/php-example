<?php

/**
 * Auxiliary function for print error message.
 *
 * @param string $output Error message.
 * @param mixed $ret_val Value to be returned.
 * @return mixed Given return value.
 */
function fail($output, $ret_val = 1) {
    print $output . PHP_EOL;
    return $ret_val;
}

class SpreadsheetCell {
    /** Regular expression for parsing formulae. */
    const formulaRegex = '/^=([A-Z]+)\(([A-Z][0-9]{1,2}):([A-Z][0-9]{1,2})\)$/';

    /** @var float|string Value of cell - either float value or formula string.  */
    private $value;

    /** @var Spreadsheet Reference to parent spreadsheet. */
    private $spreadsheet;

    /**
     * @var bool Auxiliary variable for check if cell is processed at the moment.
     * Used to avoid recursive formulae.
     */
    private $isEvaluated;

    /**
     * Check if formula is in correct format.
     *
     * @param string $formula Formula.
     * @return string|null Returns formula if the given string is in correct
     * formula format, otherwise returns null.
     */
    private function checkFormula($formula) {

        if(preg_match(SpreadsheetCell::formulaRegex, $formula)) {
            if($this->evalFormula($formula) !== null) {
                return $formula;
            }
        }

        return fail('Invalid formula', null);
    }

    /**
     * Evaluate given formula.
     *
     * @param string $formula Formula to be evaluated.
     * @return float Result of formula.
     */
    private function evalFormula($formula) {
        list($formula, $startRange, $endRange) =
            explode(',', preg_filter(SpreadsheetCell::formulaRegex, '\\1,\\2,\\3', $formula));
        $result = $this->spreadsheet->rangeOperation($formula, $startRange, $endRange);
        if($result !== null) {
            return $result;
        } else {
            return fail('Formula in bad format.', null);
        }
    }

    /**
     * Create new instance of Spreadsheet cell.
     *
     * @param Spreadsheet $spreadsheet Reference to parent Spreadsheet.
     * @param mixed $value Value to be used for this cell.
     * It can be either float (exact value) or string (formula that is
     * evaluated when reading cell value).
     */
    function __construct($spreadsheet, $value = null) {
        if($value !== null) {
            $this->setValue($value);
        } else {
            $this->value = null;
        }

        $this->spreadsheet = $spreadsheet;
        $this->isEvaluated = False;
    }

    /**
     * Get string representation of this cell.
     *
     * @return string String value of this cell.
     */
    function getStringValue() {
        if($this->isEmpty()) {
            return '';
        } else {
            return strval($this->getValue());
        }
    }

    /**
     * Get evaluated value of this cell.
     *
     * @return float|null Evaluated value of this cell
     * or null if formula is invalid.
     */
    function getValue() {
        if($this->isEvaluated) {
            $this->isEvaluated = False;
            return fail('Recursive formula.', null);
        } else {
            $this->isEvaluated = True;
        }

        if($this->isEmpty()) {
            $result = 0.0;
        } else if(is_numeric($this->value)) {
            $result = $this->value;
        } else {
            $result = $this->evalFormula($this->value);
        }

        $this->isEvaluated = False;

        return $result;
    }

    /**
     * Set value for this cell.
     *
     * @param mixed $value New value for this cell.
     * Either float (number) or string (formula).
     */
    function setValue($value) {
        if(empty($value)) {
            $this->value = null;
        } else if(is_numeric($value)) {
            $this->value = (float) $value;
        } else {
            $this->isEvaluated = True;
            $this->value = $this->checkFormula($value);
            if(!$this->isEvaluated) {
                $this->value = null;
            } else {
                $this->isEvaluated = False;
            }
        }
    }

    /**
     * Cell is empty.
     *
     * @return bool True if cell is empty, false otherwise.
     */
    function isEmpty() {
        return $this->value === null;
    }
}

class Spreadsheet {
    /** Number of spreadsheet columns. */
    const columns = 8;

    /** Number of spreadsheet rows. */
    const rows = 10;

    /** Number of spaces to be used between columns in printed output. */
    const columnSpaces = 1;

    /** Space reserved for row label. */
    const rowLabelSpace = 3;

    /** Delimiter used where reading input file with spreadsheet values. */
    const delimiter = ',';

    /** @var array(string) Array of available column labels. */
    private $columnLabels;

    /** @var array(int) Array of available column indexes (inverse array to $columnLabels). */
    private $columnNumbers;

    /** @var array(array(SpreadsheetCell)) Double dimension array with spreadsheet cells data. */
    private $data;

    /**
     * Convert column name to its corresponding index number in array.
     *
     * @param string $name Column name.
     * @return int Number indicating corresponding column index in array.
     */
    private function getColumnNumber($name) {
        if(array_key_exists($name, $this->columnNumbers)) {
            return $this->columnNumbers[$name];
        } else {
            return -1;
        }
    }

    /**
     * Auxiliary function for fetching row and column index corresponding to cell name.
     * Note: Also range check is handled here.
     *
     * @param string $cellName Name of requested cell.
     * @return array|null Array of [rowNumber, colNumber] or null if invalid name given.
     */
    private function getRowColumnByName($cellName) {
        $colRow = preg_split("/^([A-Z])([0-9]+)$/", $cellName, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

        if(count($colRow) != 2) {
            return null;
        }

        list($colName, $rowNumber) = $colRow;

        $columnNumber =$this->getColumnNumber($colName);
        $rowNumber -= 1;

        if($rowNumber < 0 || $rowNumber > Spreadsheet::rows - 1) {
            return null;
        }

        return array($rowNumber, $columnNumber);
    }

    /**
     * Return cell instance by its name.
     *
     * @param string $cellName Name of requested cell.
     * @return SpreadsheetCell Instance of SpreadsheetCell or null if invalid name given.
     */
    private function getCellByName($cellName)
    {
        if(($rowCol = $this->getRowColumnByName($cellName)) === null) {
            return null;
        }

        list($row, $col) = $rowCol;
        return $this->data[$row][$col];
    }

    /**
     * Return array of cells corresponding to given range.
     *
     * @param string $cellNameStart Name of top left cell in range.
     * @param string $cellNameEnd Name of bottom right cell in range.
     * @return array|null Array of cells in given range or null if invalid range given.
     */
    private function getCellRange($cellNameStart, $cellNameEnd) {
        if(($rowCol1 = $this->getRowColumnByName($cellNameStart)) === null) {
            return null;
        }

        if(($rowCol2 = $this->getRowColumnByName($cellNameEnd)) === null) {
            return null;
        }

        list($row1, $col1) = $rowCol1;
        list($row2, $col2) = $rowCol2;

        if($col2 < 0 || $col2 > Spreadsheet::columns - 1 || $row2 < 0 || $row2 > Spreadsheet::rows - 1) {
            return null;
        }

        if($row1 > $row2 || $col1 > $col2) {
            return null;
        }

        $cells = array();

        for($itRow = $row1; $itRow <= $row2; $itRow++) {
            for($itCol = $col1; $itCol <= $col2; $itCol++) {
                $cells[] = $this->data[$itRow][$itCol];
            }
        }

        return $cells;
    }

    /**
     * Worker function for range operation.
     *
     * @param string $operationMethod Name of operation to be called.
     * @param string $cellNameStart Name of top left cell in range.
     * @param string $cellNameEnd Name of bottom right cell in range.
     * @return float Result of range operation.
     */
    private function rangeOperationProc($operationMethod, $cellNameStart, $cellNameEnd) {
        $cells = $this->getCellRange($cellNameStart, $cellNameEnd);

        if($cells === null) {
            return fail('Invalid cell range given.', null);
        }

        return call_user_func(array(get_class($this), $operationMethod), $cells);
    }

    /**
     * Sum of cells range operation.
     *
     * @param array(SpreadsheetCell) $cells Cells to be evaluated.
     * @return float Result of operation.
     */
    private function sumCells($cells) {
        $result = 0.0;

        foreach($cells as $cell) {
            $result += $cell->getValue();
        }

        return $result;
    }

    /**
     * Min from cells range operation.
     *
     * @param array(SpreadsheetCell) $cells Cells to be evaluated.
     * @return float Result of operation.
     */
    private function minCells($cells) {
        $result = INF;

        foreach($cells as $cell) {
            $value = !$cell->isEmpty() ? $cell->getValue() : INF;
            $result = min($result, $value);
        }

        return $result;
    }

    /**
     * Max from cells range operation.
     *
     * @param array(SpreadsheetCell) $cells Cells to be evaluated.
     * @return float Result of operation.
     */
    private function maxCells($cells) {
        $result = -INF;

        foreach($cells as $cell) {
            $value = !$cell->isEmpty() ? $cell->getValue() : -INF;
            $result = max($result, $value);
        }

        return $result;
    }

    /**
     * Loop through rows in given column and compute space
     * to be reserved for this column in printed output.
     *
     * @param string $columnIndex Index of column.
     * @return int Space to be reserved for column.
     */
    private function computeColumnSize($columnIndex) {
        $tempMaxSize = 1;

        for($row = 0; $row < Spreadsheet::rows; $row++) {
            $value = $this->data[$row][$columnIndex]->getStringValue();
            $tempMaxSize = max($tempMaxSize, strlen($value));
        }

        return $tempMaxSize;
    }

    /**
     * Create new instance of Spreadsheet.
     * @param string $fileName Filename from which should be spreadsheet loaded.
     */
    function __construct($fileName = null) {
        $this->columnLabels = range('A', 'Z');
        $this->columnNumbers = array_flip($this->columnLabels);

        for($row = 0; $row < Spreadsheet::rows; $row++) {
            $rowArray = array();
            for($col = 0; $col < Spreadsheet::columns; $col++) {
                $rowArray[] = new SpreadsheetCell($this);
            }
            $this->data[] = $rowArray;
        }

        if($fileName !== null) {
            $this->loadFromFile($fileName);
        }
    }

    /**
     * Get evaluated value of cell.
     * @param string $cellName Name of requested cell.
     * @return float Evaluated value of given cell.
     */
    function getCellValue($cellName) {
        if(($cell = $this->getCellByName($cellName)) !== null) {
            return $cell->getValue();
        } else {
            return fail('Invalid cell name given.', null);
        }
    }

    /**
     * Get exact string value of cell.
     * @param string $cellName Name of requested cell.
     * @return string String representation of given cell.
     */
    function getCellStringValue($cellName) {
        if(($cell = $this->getCellByName($cellName)) !== null) {
            return $cell->getStringValue();
        } else {
            return fail('Invalid cell name given.', null);
        }
    }

    /**
     * Set cell to specified value.
     * @param string $cellName Name of cell to be set.
     * @param mixed $value Value to be set - either number or formula.
     */
    function setCellValue($cellName, $value) {
        if(($cell = $this->getCellByName($cellName)) !== null) {
            $cell->setValue($value);
        } else {
            fail('Invalid cell name given.');
        }
    }

    /**
     * Clear value of specified cell.
     * @param string $cellName Name of cell to be cleared.
     */
    function clearCellValue($cellName) {
        $this->setCellValue($cellName, '');
    }


    /**
     * Perform range operation.
     *
     * @param string $funcName Name of operation to be evaluated on cell range.
     * @param string $rangeStart Name of top left cell in range.
     * @param string $rangeEnd Name of bottom right cell in range.
     * @return float|null Result of range operation or null if invalid operation
     * name given.
     */
    function rangeOperation($funcName, $rangeStart, $rangeEnd) {
        switch($funcName) {
            case 'SUM':
                return $this->rangeOperationProc('sumCells', $rangeStart, $rangeEnd);
            case 'MAX':
                return $this->rangeOperationProc('maxCells', $rangeStart, $rangeEnd);
            case 'MIN':
                return $this->rangeOperationProc('minCells', $rangeStart, $rangeEnd);
            default:
                return fail('Invalid function name given.', null);
        }
    }

    /**
     * Worker procedure for loading spreadsheet from file.
     *
     * @param resource $file File pointer.
     * @return int 0 if everything went fine, 1 otherwise.
     */
    private function loadFromFileProc($file) {
        // Read header
        if(($firstLine = fgets($file)) !== false) {
            $rowStr = trim($firstLine);
            if(is_numeric($rowStr)) {
                $rows = (int) $rowStr;
            } else {
                return fail('Invalid size in spreadsheet file.');
            }
        } else {
            return fail('Spreadsheet file without header.');
        }

        // Read data
        for($rowIndex = 0; $rowIndex < $rows; $rowIndex++) {
            if (($row = fgets($file)) !== false) {
                $rowData = explode(Spreadsheet::delimiter, trim($row));
                if(count($rowData) > Spreadsheet::columns) {
                    return fail('Invalid format of spreadsheet file.');
                }
                foreach ($rowData as $columnIndex=>$cellData) {
                    $cellName = $this->columnLabels[$columnIndex] . ($rowIndex + 1);
                    $this->setCellValue($cellName, $cellData);
                }
            } else {
                return fail('Invalid format of spreadsheet file.');
            }
        }

        return 0;
    }

    /**
     * Load spreadsheet from file.
     *
     * @param string $fileName Name of file with spreadsheet.
     * @return int 0 if everything went fine, 1 otherwise.
     */
    function loadFromFile($fileName) {
        if(file_exists($fileName)) {
            $file = fopen($fileName, "r");
        } else {
            return fail('File does not exist.');
        }

        $ret_val = $this->loadFromFileProc($file);

        fclose($file);
        return $ret_val;
    }

    /**
     * Print spreadsheet to output.
     */
    function printSheet() {
        $printRow = str_repeat(' ', Spreadsheet::rowLabelSpace);

        $columnSizes = array();
        for($columnIndex = 0; $columnIndex < Spreadsheet::columns; $columnIndex++) {
            $columnSizes[$columnIndex] = $this->computeColumnSize($columnIndex);
            $printRow .= str_repeat(' ', Spreadsheet::columnSpaces);
            $printRow .= $this->columnLabels[$columnIndex];
            $printRow .= str_repeat(' ', $columnSizes[$columnIndex] - 1);
        }

        print $printRow . PHP_EOL;

        for($rowIndex = 0; $rowIndex < Spreadsheet::rows; $rowIndex++) {
            $printRow = $rowIndex+1;
            $printRow .= str_repeat(' ', Spreadsheet::rowLabelSpace - strlen($rowIndex+1));
            for($columnIndex = 0; $columnIndex < Spreadsheet::columns; $columnIndex++) {
                $cellName = $this->columnLabels[$columnIndex] . ($rowIndex + 1);
                $cellData = $this->getCellStringValue($cellName);
                $columnPad = $columnSizes[$columnIndex] - strlen($cellData);
                $printRow .= str_repeat(' ', Spreadsheet::columnSpaces + $columnPad);
                $printRow .= $cellData;
            }
            print $printRow . PHP_EOL;
        }
    }
}