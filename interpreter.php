<?php

require 'spreadsheet.php';

$hintMessage =
"
Simple Spreadsheet processor

Available commands:

load <filename> - load spreadsheet from file
print - print current spreadsheet

get <cellName> - get cell value
set <cellName> <value> - set cell value
clr <cellName> - clear cell

exit - exit program

";

$spreadsheet = new Spreadsheet();

echo $hintMessage;

while($line = fgets(STDIN)) {
    $elems = explode(" ", trim($line));
    $command = array_shift($elems);

    switch($command) {
        case 'load':
            if(count($elems) >= 1) {
                $fileName = $elems[0];
                $spreadsheet->loadFromFile($fileName);
            } else {
                fail('Invalid command parameters.');
            }
            break;
        case 'print':
            $spreadsheet->printSheet();
            break;
        case 'get':
            if(count($elems) >= 1) {
                $cellName = $elems[0];
                if(($cellValue = $spreadsheet->getCellValue($cellName)) !== null) {
                    printf("Cell %s: %f\n", $cellName, $cellValue);
                }
            } else {
                fail('Invalid command parameters.');
            }
            break;
        case 'set':
            if(count($elems) >= 2) {
                list($cellName, $value) = $elems;
                $spreadsheet->setCellValue($cellName, $value);
            } else {
                fail('Invalid command parameters.');
            }
            break;
        case 'clr':
            if(count($elems) >= 1) {
                $cellName = $elems[0];
                $spreadsheet->clearCellValue($cellName);
            } else {
                fail('Invalid command parameters.');
            }
            break;
        case 'exit':
            exit(0);
            break;
        default:
            echo $hintMessage;
    }
}